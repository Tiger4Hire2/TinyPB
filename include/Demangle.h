#pragma once
#include <string>
#include <string_view>

// Unfortunately generating type names requires RTTI, which is a BAD option to turn on.
// That said, this turning it on for a few modules, which do not use the worst features is forgivable (I hope)

#ifdef __linux__
#include <cxxabi.h>
template<class T>
std::string Demangle()
{
    int       status;
    char*     realname;
    const auto id_name = typeid(T).name();
    realname = abi::__cxa_demangle(id_name, 0, 0, &status);
    std::string retval{realname};
    free(realname);
    const auto last = retval.find_last_of(':');
    if (last!=std::string::npos)
        return retval.substr(last+1);
    return retval;
}
#elif defined(WIN32)
// windows typeid are generally more readable, but you may want to remove "struct "
// I will not attempt that "blind", though (I cannot compile on windows ATM)
template<class T>
std::string Demangle()
{
    const auto id_name = typeid<T>.name();
    // removed struct
    return std::string{id_name};
}
#endif
